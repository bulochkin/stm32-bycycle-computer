/**
  ******************************************************************************
  * File Name          : main.h
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  *
  * Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H
  /* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define LED_Pin GPIO_PIN_13
#define LED_GPIO_Port GPIOC
#define SpeedFront_Pin GPIO_PIN_2
#define SpeedFront_GPIO_Port GPIOA
#define SpeedRear_Pin GPIO_PIN_3
#define SpeedRear_GPIO_Port GPIOA
#define BackButton_Pin GPIO_PIN_1
#define BackButton_GPIO_Port GPIOA
#define MenuButton_Pin GPIO_PIN_0
#define MenuButton_GPIO_Port GPIOA

#define LCD_MOSI_Pin GPIO_PIN_3
#define LCD_MOSI_GPIO_Port GPIOB
#define LCD_SCK_Pin GPIO_PIN_4
#define LCD_SCK_GPIO_Port GPIOB
#define LCD_DC_Pin GPIO_PIN_5
#define LCD_DC_GPIO_Port GPIOB
#define LCD_RST_Pin GPIO_PIN_6
#define LCD_RST_GPIO_Port GPIOB
#define LCD_CS_Pin GPIO_PIN_7
#define LCD_CS_GPIO_Port GPIOB
#define LCD_LED_PWR_Pin GPIO_PIN_8
#define LCD_LED_PWR_GPIO_Port GPIOB
#define LCD_PWR_Pin GPIO_PIN_9
#define LCD_PWR_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */
#define STATE_MAIN_SPEED 0
#define STATE_MAIN_DISTANCE 1
#define STATE_MENU 2
#define STATE_LAST_TRAVEL 3
#define STATE_OVERALL_STAT 4
#define STATE_STAT_SPEED 5
#define STATE_STAT_RPM 6
#define STATE_STAT_DISTANCE 7
#define STATE_STAT_TIME 8
#define STATE_SYNC_WAIT 9
#define STATE_SYNC_RUNNING 10
#define STATE_SYNC_COMPLETED 11
#define STATE_SETTINGS 12
#define STATE_SETTINGS_BACKLIGHT_MM 13
#define STATE_SETTINGS_BACKLIGHT_SS 14
#define STATE_SETTINGS_AUTOSLEEP_MM 15
#define STATE_SETTINGS_AUTOSLEEP_SS 16
#define STATE_SETTINGS_CLEAR_STAT 17
#define STATE_SETTINGS_SET_TIME_HH 18
#define STATE_SETTINGS_SET_TIME_MM 19
#define STATE_YES_NO 20

#define USB_overallDuration 0
#define USB_overallDistance 1
#define USB_maxSpeed 2
#define USB_maxRpm 3
#define USB_maxDistance 4
#define USB_maxDuration 5

#define LAST_TRAVEL_DISTANCE_ADDRESS 0x00
#define LAST_TRAVEL_DURATION_ADDRESS 0x02
#define OVERALL_DURATION_ADDRESS 0x04 //WORD
#define OVERALL_DISTANCE_ADDRESS 0x08 //WORD
#define MAX_SPEED_ADDRESS 0x0C
#define MAX_RPM_ADDRESS 0x0E
#define MAX_DISTANCE_ADDRESS 0x10
#define MAX_DURATION_ADDRESS 0x12
#define BACKLIGHT_TIME_ADDRESS 0x14
#define AUTOSLEEP_TIME_ADDRESS 0x16
/* USER CODE END Private defines */

/**
  * @}
  */ 

/**
  * @}
*/ 

#endif /* __MAIN_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

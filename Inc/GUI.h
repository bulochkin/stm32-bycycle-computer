#ifndef __GUI_H
#define __GUI_H

    /* exact-width signed integer types */
typedef   signed          char int8_t;
typedef   signed short     int int16_t;
typedef   signed           int int32_t;

    /* exact-width unsigned integer types */
typedef unsigned          char uint8_t;
typedef unsigned short     int uint16_t;
typedef unsigned           int uint32_t;

#define MENU_MAIN 0
#define MENU_SETTINGS 1
#define MENU_MAIN_IN_TRAVEL 2
#define STAT_DISTANCE 0
#define STAT_TIME 1
#define STAT_SPEED 2
#define STAT_RPM 3

static char* menuitems[6] = {"Begin travel", "Last travel", "Overall stat", "Statistics", "Sync with PC", "Settings"};
static char* settingsitems[4] = {"Backlight", "Autosleep", "Clear all stats", "Set time"};

void DrawTopBar(uint8_t hours, uint8_t minutes, uint8_t seconds, uint8_t wkup);
void DrawBotBar(char* first, char* second);
void DrawMainScreenSpeed(uint8_t speed, uint8_t rpm);
void DrawScreenTravel(char* title, uint32_t dist, uint32_t time);
void DrawTimeDialog(char* title, uint8_t first, uint8_t second, uint8_t blink);
void DrawMenuList(uint8_t menu, uint8_t selected);
void DrawStatScreen(char* title, uint32_t value, uint8_t type);
void DrawUSBSyncTitleScreen(void);
void DrawUSBSyncDoneScreen(void);
void DrawUSBSyncProgressScreen(uint8_t progress);
void DrawYesNoDialogScreen(uint8_t state);
void DrawStartupSplashScreen(uint8_t number);
void DrawShutdownSplashScreen(void);
void DrawTestScreen(void);
#endif

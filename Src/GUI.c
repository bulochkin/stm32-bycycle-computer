#include "GUI.h"
#include "PCF8812.h"
#include "font5x7.h"
#include <stm32f1xx_hal.h>


uint8_t strLen(char* str)
{
	uint8_t res = 0;
	char* tmp = str;
	while(*tmp != '\0')
	{
		tmp++;
		res++;
	}
	return res;
}

int num = 0;

void DrawTopBar(uint8_t hours, uint8_t minutes, uint8_t seconds, uint8_t wkup)
{
	PCF8812_HLine(1, 101, 9, PSet);
	PCF8812_VLine(1, 1, 9, PSet);
	PCF8812_VLine(101, 1, 9, PSet);
	PCF8812_PutInt5x7Aligned(2, 5, 0, hours, transparent);
	PCF8812_PutChar5x7(16, 0, ':', transparent);
	PCF8812_PutInt5x7Aligned(2, 21, 0, minutes, transparent);
	PCF8812_PutChar5x7(33, 0, ':', transparent);
	PCF8812_PutInt5x7Aligned(2, 38, 0, seconds, transparent);	
	if (wkup == 1)
		PCF8812_PutChar5x7(60, 0, '*', transparent);
	//battery
	PCF8812_Rect(87, 0, 99, 7, PSet);
	//for (int i = 0; i < num; i+=10)
		//PCF8812_FillRect(88+i/10*2, 1, 88+(i/10+1)*2, 6, PSet);
	PCF8812_FillRect(88, 1, 88+(num/10+1)*2, 6, PSet);
	if (++num >= 50)
		num = 0;
}

void DrawBotBar(char* first, char* second)
{
	uint8_t firstLen = strLen(first);
	uint8_t secondLen = strLen(second);
	if (firstLen != 0)
	{
		PCF8812_Rect(2, 54, 2+firstLen*6+4, 63, PSet);
		PCF8812_PutStr5x7(4, 55, first, transparent);
	}
	if (secondLen != 0)
	{
		PCF8812_Rect(101-secondLen*6-4, 54, 101, 63, PSet);
		PCF8812_PutStr5x7(101-secondLen*6-1, 55, second, transparent);
	}
}


void DrawMainScreenSpeed(uint8_t speed, uint8_t rpm)
{
	PCF8812_PutInt5x7ScaledAligned(3, 3, 2, 11, speed, opaque);
	PCF8812_PutStr5x7Scaled(2, 54, 18, "km/h", opaque);
	PCF8812_PutInt5x7ScaledAligned(3, 2, 2, 34, rpm, opaque);
	PCF8812_PutStr5x7Scaled(2, 66, 34, "rpm", opaque);
}
uint8_t hour, min;
void DrawScreenTravel(char* title, uint32_t dist, uint32_t time)
{
	PCF8812_PutStr5x7(2, 12, title, transparent);
	PCF8812_PutInt5x7ScaledAligned(5, 2, 2, 21, dist, opaque);
	PCF8812_PutStr5x7Scaled(2, 90, 21, "m", opaque);
	if (time != 0)
	{
		hour = time / 3600;
		time -= hour * 3600;
		if (time != 0)
		{
			min = time / 60;
			time -= min * 60;
		}
	}
	PCF8812_PutInt5x7ScaledAligned(2, 2, 2, 38, hour, opaque);
	PCF8812_PutChar5x7Scaled(2, 26, 38, ':', opaque);
	PCF8812_PutInt5x7ScaledAligned(2, 2, 38, 38, min, opaque);
	PCF8812_PutChar5x7Scaled(2, 62, 38, ':', opaque);
	PCF8812_PutInt5x7ScaledAligned(2, 2, 74, 38, time, opaque);
}

void DrawMenuList(uint8_t menu, uint8_t selected)
{
	uint8_t base = 0;
	if (selected > 3 )
		base = selected - 3;
	char** selectedMenu;
	switch(menu)
	{
		case MENU_MAIN:
		case MENU_MAIN_IN_TRAVEL:
		selectedMenu = menuitems;
		break;
		case MENU_SETTINGS:
		selectedMenu = settingsitems;
		break;
	}
	for (int i = 0; i < 4; i++)
	{
		if (menu == MENU_MAIN_IN_TRAVEL && (i+base) == 0)
			PCF8812_PutStr5x7(4, 12 + i*10, "End Travel", transparent);
		else
			PCF8812_PutStr5x7(4, 12 + i*10, selectedMenu[i+base], transparent);
	}
	PCF8812_Rect(2, (selected - base) * 10 + 10, 101, (selected - base) * 10 + 21, PSet);
}

void DrawTimeDialog(char* title, uint8_t first, uint8_t second, uint8_t blink)
{
		PCF8812_PutStr5x7(2, 12, title, transparent);	
		if (blink != 1) PCF8812_PutInt5x7ScaledAligned(2, 3, 8, 25, first, opaque);
		PCF8812_PutChar5x7Scaled(3, 46, 25, ':', opaque);
		if (blink != 2) PCF8812_PutInt5x7ScaledAligned(2, 3, 66, 25, second, opaque);
}

void DrawStatScreen(char* title, uint32_t value, uint8_t type)
{
		PCF8812_PutStr5x7(2, 12, "STAT", opaque);
		PCF8812_PutStr5x7(2, 20, title, opaque);
		if (type == STAT_DISTANCE)
		{
			PCF8812_PutInt5x7ScaledAligned(5, 2, 2, 30, value, opaque);
			PCF8812_PutStr5x7Scaled(2, 64, 30, "m", opaque);
		} else if (type == STAT_TIME)
		{
			if (value != 0)
			{
				hour = value / 3600;
				value -= hour * 3600;
				if (value != 0)
				{
					min = value / 60;
					value -= min * 60;
				}
			}
			PCF8812_PutInt5x7ScaledAligned(2, 2, 2, 30, hour, opaque);
			PCF8812_PutChar5x7Scaled(2, 26, 30, ':', opaque);
			PCF8812_PutInt5x7ScaledAligned(2, 2, 38, 30, min, opaque);
			PCF8812_PutChar5x7Scaled(2, 62, 30, ':', opaque);
			PCF8812_PutInt5x7ScaledAligned(2, 2, 74, 30, value, opaque);
		} else if (type == STAT_SPEED)
		{
			PCF8812_PutInt5x7ScaledAligned(3, 2, 2, 30, value, opaque);
			PCF8812_PutStr5x7Scaled(2, 50, 30, "km/h", opaque);
		}else if (type == STAT_RPM)
		{
			PCF8812_PutInt5x7ScaledAligned(3, 2, 2, 30, value, opaque);
		}
}

void DrawUSBSyncTitleScreen(void)
{
	PCF8812_PutStr5x7Scaled(2, 2, 12, "USB sync", opaque);
	PCF8812_PutStr5x7(2, 30, "connect usb cable and run app", opaque);
}

void DrawUSBSyncDoneScreen(void)
{
	PCF8812_PutStr5x7Scaled(2, 2, 12, "USB sync", opaque);
	PCF8812_PutStr5x7(2, 30, "completed!", opaque);
}


void DrawUSBSyncProgressScreen(uint8_t progress)
{
	PCF8812_PutStr5x7Scaled(2, 2, 12, "USB sync", opaque);
	PCF8812_Rect(2, 30, 101, 50, PSet);
	PCF8812_FillRect(2, 30, progress+2, 50, PSet);
}

void DrawYesNoDialogScreen(uint8_t state)
{
	PCF8812_PutStr5x7(2, 11, "Clear ", opaque);
	switch(state)
	{
		case STATE_OVERALL_STAT:
			PCF8812_PutStr5x7(40, 11, "overall stats?", opaque);	
			break;
		case STATE_STAT_SPEED:			
			PCF8812_PutStr5x7(40, 11, "max speed?", opaque);	
			break;
		case STATE_STAT_RPM:
			PCF8812_PutStr5x7(40, 11, "max rpm?", opaque);	
			break;
		case STATE_STAT_DISTANCE:
			PCF8812_PutStr5x7(40, 11, "max distance?", opaque);	
			break;
		case STATE_STAT_TIME:
			PCF8812_PutStr5x7(40, 11, "max duration?", opaque);	
			break;
		case STATE_SETTINGS_CLEAR_STAT:
			PCF8812_PutStr5x7(40, 11, "ALL stats?", opaque);	
			break;
	}
	/*PCF8812_Rect(7, 22, 47, 46, PSet);
	PCF8812_PutStr5x7Scaled(2, 10, 28, "YES", opaque);
	PCF8812_Rect(57, 22, 97, 46, PSet);
	PCF8812_PutStr5x7Scaled(2, 67, 28, "NO", opaque);
	PCF8812_Rect(5+50*selected, 20, 49+50*selected, 48, PSet);*/
}

void DrawStartupSplashScreen(uint8_t number)
{
	if (number == 0)
		PCF8812_FlushImage();	
	else if (number == 1)
		PCF8812_FlushImageOSH();		
	//PCF8812_PutStr5x7Scaled(3, 10, 20, "HELLO", opaque);
}

void DrawShutdownSplashScreen(void)
{
	PCF8812_PutStr5x7Scaled(3, 20, 20, "BYE!", opaque);
}

void DrawTestScreen(void)
{
	if (num >= 0 && num < 12)
	{
		PCF8812_SetPixel(1,0);
		PCF8812_PutChar5x7(20, 20, '1', transparent);
	}
	else if (num >= 12 && num < 24)
	{
		PCF8812_SetPixel(101, 0);
		PCF8812_PutChar5x7(20, 20, '2', transparent);
	}
	else if (num >= 24 && num < 36)
	{
		PCF8812_SetPixel(1,63);
		PCF8812_PutChar5x7(20, 20, '3', transparent);
	}
	else if (num >= 36 && num < 48)
	{
		PCF8812_SetPixel(101, 63);
		PCF8812_PutChar5x7(20, 20, '4', transparent);
	}
}

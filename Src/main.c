/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"
#include "cmsis_os.h"
#include "usb_device.h"

/* USER CODE BEGIN Includes */
extern USBD_HandleTypeDef  hUsbDeviceFS;
#include "usbd_customhid.h"
#include "PCF8812.h"
#include "font5x7.h"
#include "GUI.h"
#include "eeprom.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
SPI_HandleTypeDef hspi1;
SPI_HandleTypeDef hspi2;

TIM_HandleTypeDef htim3;

RTC_HandleTypeDef hrtc;

osThreadId defaultTaskHandle;
osThreadId usbTaskHandle;

osTimerId countSpeedTimerHandle;
osTimerId everySecondTimerHandle;

osMutexId accessSpeedMutexHandle;
osMutexId accessUsbMutexHandle;

osSemaphoreId menuPressedSemaphoreHandle;
osSemaphoreId backPressedSemaphoreHandle;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
#define WheelFactor 7560
#define WheelLength 2.1
// speed data
float Speed;
unsigned short frontRPM;
unsigned short rearRPM;
// io data
TickType_t menuLast = 0;
TickType_t backLast = 0;
TickType_t frontLast = 0;
TickType_t frontLast1 = 0;
TickType_t frontLast2 = 0;
TickType_t rearLast = 0;
TickType_t rearLast1 = 0;
TickType_t rearLast2 = 0;
// travel data
volatile uint32_t currentTravelStarted;
uint32_t currentTravelDuration;
float currentTravelDistance;
// need to be persist --->
uint16_t lastTravelDistance;
uint16_t lastTravelDuration;
volatile uint32_t overallDuration;
volatile uint32_t overallDistance;
uint8_t maxSpeed;
uint8_t maxRpm;
volatile uint16_t maxDistance;
volatile uint16_t maxDuration;
uint16_t backlightTime=30;
uint16_t autosleepTime=30;
// <----
// ui data
uint8_t state = STATE_MAIN_SPEED;
uint8_t prevState;
uint8_t menu = 0; 
uint8_t back = 0;
uint8_t usbSyncProgress = 0;
uint8_t wkup = 0;
volatile uint8_t powerOffHold = 0;
volatile uint8_t inactiveTime = 0;
uint16_t tempTime = 0;
//rtc
RTC_TimeTypeDef sTime;
RTC_DateTypeDef sDate;
//speed count task
TickType_t frontLastFixed = 0; 
TickType_t rearLastFixed = 0;
TickType_t current = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);
static void MX_GPIO_Init(void);
static void MX_TIM3_Init(void);
static void MX_SPI2_Init(void);
static void MX_SPI1_Init(void);
void StartDefaultTask(void const * argument);
void UsbTask(void const * argument);
void countSpeedTimeElapsed(void const * argument);
void everySecondTimeElapsed(void const * argument);
static void MX_NVIC_Init(void);
static void MX_RTC_Init(void);
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin);
void PowerOff(void);
void GoToSleep(void);
/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	TickType_t current = xTaskGetTickCountFromISR();
	inactiveTime = 0;
	if (GPIO_Pin == MenuButton_Pin)
	{
		if (HAL_GPIO_ReadPin(MenuButton_GPIO_Port, MenuButton_Pin) == 0)
		{
			powerOffHold = 0;
			return;
		}
		if (current < (menuLast + 250))
			return;
		xSemaphoreGiveFromISR(menuPressedSemaphoreHandle, NULL);
		menuLast = current;
	} else if (GPIO_Pin == BackButton_Pin)
	{
		if (current < (backLast + 250))
			return;
		xSemaphoreGiveFromISR(backPressedSemaphoreHandle, NULL);
		backLast = current;
		return;
	} else if (GPIO_Pin == SpeedFront_Pin)
	{
		if (current < (frontLast + 100))
			return;
		if (frontLast == 0)
		{
			frontLast = current;
			return;
		}
		frontLast2 = frontLast1;
		frontLast1 = current - frontLast;
		frontLast = current;
		return;
	} else if (GPIO_Pin == SpeedRear_Pin)
	{
		if (current < (rearLast + 100))
			return;
		if (currentTravelStarted != 0)
		{
			currentTravelDistance += WheelLength;
			overallDistance += WheelLength;
			if (currentTravelDistance > maxDistance)
				maxDistance = currentTravelDistance;
		}
		if (rearLast == 0)
		{
			rearLast = current;
			return;
		}
		rearLast2 = rearLast1;
		rearLast1 = current - rearLast;
		rearLast = current;
		return;
	}
}

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();
	
	
	DBGMCU->CR |= DBGMCU_CR_DBG_STANDBY;
	wkup = (PWR->CSR & PWR_CSR_WUF) == PWR_CSR_WUF;

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM3_Init();
  //MX_SPI2_Init();
  //MX_SPI1_Init();

  MX_RTC_Init();
  /* Initialize interrupts */
  MX_NVIC_Init();

  /* USER CODE BEGIN 2 */		
	PCF8812_Init();
	PCF8812_PowerOn();
	PCF8812_Initialize();
	if (wkup == 0)
	{
		DrawStartupSplashScreen(0);
		HAL_Delay(1500);
	}
  /* init code for USB_DEVICE */
  MX_USB_DEVICE_Init();
	
  /* USER CODE END 2 */

  /* Create the mutex(es) */
  /* definition and creation of accessSpeedMutex */
  osMutexDef(accessSpeedMutex);
  accessSpeedMutexHandle = osMutexCreate(osMutex(accessSpeedMutex));
  /* definition and creation of accessUsbMutex */
  osMutexDef(accessUsbMutex);
  accessUsbMutexHandle = osMutexCreate(osMutex(accessUsbMutex));

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* Create the semaphores(s) */
  osSemaphoreDef(menuPressedSemaphore);
  menuPressedSemaphoreHandle = osSemaphoreCreate(osSemaphore(menuPressedSemaphore), 1);
  osSemaphoreDef(backPressedSemaphore);
  backPressedSemaphoreHandle = osSemaphoreCreate(osSemaphore(backPressedSemaphore), 1);

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* Create the timer(s) */
  /* definition and creation of countSpeedTimer */
	osTimerDef(countSpeedTimer, countSpeedTimeElapsed);
  countSpeedTimerHandle = osTimerCreate(osTimer(countSpeedTimer), osTimerPeriodic, NULL);
  /* definition and creation of everySecondTimer */
	osTimerDef(everySecondTimer, everySecondTimeElapsed);
  everySecondTimerHandle = osTimerCreate(osTimer(everySecondTimer), osTimerPeriodic, NULL);

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityAboveNormal, 0, 64);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of usbTask */
  osThreadDef(usbTask, UsbTask, osPriorityHigh, 0, 64);
  usbTaskHandle = osThreadCreate(osThread(usbTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */
 	
	if (wkup == 0)
	{
		DrawStartupSplashScreen(1);
		HAL_Delay(1500);
	}
  /* Start scheduler */
  osKernelStart();
  
  /* We should never get here as control is now taken by the scheduler */

  /* USER CODE BEGIN 3 */
			
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE|RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_USB;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL_DIV1_5;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);
}


/** NVIC Configuration
*/
static void MX_NVIC_Init(void)
{
  /* EXTI0_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(EXTI0_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);
	/* EXTI3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(EXTI3_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(EXTI3_IRQn);
	/* EXTI1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(EXTI1_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(EXTI1_IRQn);
	/* EXTI2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(EXTI2_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(EXTI2_IRQn);
}


/* SPI1 init function */
static void MX_SPI1_Init(void)
{
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_1LINE;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_64;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_LSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
	SET_BIT(SPI1->CR1, SPI_CR1_SSI);
}


/* SPI2 init function */
static void MX_SPI2_Init(void)
{
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi2.Init.NSS = SPI_NSS_HARD_OUTPUT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }

}


/* TIM3 init function */
static void MX_TIM3_Init(void)
{

  TIM_Encoder_InitTypeDef sConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 0;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 100;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  sConfig.EncoderMode = TIM_ENCODERMODE_TI2;
  sConfig.IC1Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC1Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC1Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC1Filter = 10;
  sConfig.IC2Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC2Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC2Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC2Filter = 10;
  if (HAL_TIM_Encoder_Init(&htim3, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }

}


/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
        * Free pins are configured automatically as Analog (this feature is enabled through 
        * the Code Generation settings)
*/
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LCD_DC_GPIO_Port, LCD_DC_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LCD_RST_Pin|LCD_CS_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin : LED_Pin */
  GPIO_InitStruct.Pin = LED_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LED_GPIO_Port, &GPIO_InitStruct);
	
	GPIO_InitStruct.Pin = GPIO_PIN_4;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PA5 
                           PA8 PA9 PA10  PA15*/
  GPIO_InitStruct.Pin = GPIO_PIN_5 
                          |GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : SpeedFront_Pin SpeedRear_Pin */
  GPIO_InitStruct.Pin = SpeedFront_Pin|SpeedRear_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PB0 PB1 PB2 PB10 
                           PB11 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_10 
                          |GPIO_PIN_11;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : BackButton_Pin */
  GPIO_InitStruct.Pin = BackButton_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(BackButton_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : MenuButton_Pin */
  GPIO_InitStruct.Pin = MenuButton_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(MenuButton_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LCD_DC_Pin LCD_RST_Pin LCD_CS_Pin LCD_PWR_Pin LCD_LED_PWR_Pin*/
  GPIO_InitStruct.Pin = LCD_DC_Pin|LCD_RST_Pin|LCD_CS_Pin|LCD_MOSI_Pin|LCD_SCK_Pin|LCD_PWR_Pin|LCD_LED_PWR_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	HAL_GPIO_WritePin(LCD_LED_PWR_GPIO_Port, LCD_LED_PWR_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LCD_PWR_GPIO_Port, LCD_PWR_Pin, GPIO_PIN_RESET);
}

/* RTC init function */
static void MX_RTC_Init(void)
{

    /**Initialize RTC Only 
    */
	RCC->BDCR |= RCC_BDCR_RTCEN;
  hrtc.Instance = RTC;
  hrtc.Init.AsynchPrediv = RTC_AUTO_1_SECOND;
  hrtc.Init.OutPut = RTC_OUTPUTSOURCE_NONE;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }

    /**Initialize RTC and set the Time and Date 
    */
  if(HAL_RTCEx_BKUPRead(&hrtc, RTC_BKP_DR1) != 0x32F2){
  sTime.Hours = 0x1;
  sTime.Minutes = 0x0;
  sTime.Seconds = 0x0;

  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }

  sDate.WeekDay = RTC_WEEKDAY_MONDAY;
  sDate.Month = RTC_MONTH_JANUARY;
  sDate.Date = 0x1;
  sDate.Year = 0x0;

  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }

    HAL_RTCEx_BKUPWrite(&hrtc,RTC_BKP_DR1,0x32F2);
  }

}


/* USER CODE BEGIN 4 */
void sendDataOverUSB(void)
{
	while(1)
	{
		if (xSemaphoreTake(accessUsbMutexHandle, (TickType_t) 10) == pdTRUE)
		{
			USBD_CUSTOM_HID_HandleTypeDef     *hhid = (USBD_CUSTOM_HID_HandleTypeDef*)hUsbDeviceFS.pClassData;
			static unsigned char dataToSend[6];
			dataToSend[0] = 0x02;
			/*uint32_t overallDuration;
			uint32_t overallDistance;
			uint8_t maxSpeed;
			uint8_t maxRpm;
			uint16_t maxDistance;
			uint16_t maxDuration;*/	
			dataToSend[1] = USB_overallDuration;
			dataToSend[2] = overallDuration;
			dataToSend[3] = overallDuration >> 8;
			dataToSend[4] = overallDuration >> 16;
			dataToSend[5] = overallDuration >> 24;
			while(hhid->state != CUSTOM_HID_IDLE) osDelay(1);
			USBD_CUSTOM_HID_SendReport(&hUsbDeviceFS, dataToSend, 6);
			while(hhid->state != CUSTOM_HID_IDLE) osDelay(1);
			usbSyncProgress = 16;
			dataToSend[1] = USB_overallDistance;
			dataToSend[2] = overallDistance;
			dataToSend[3] = overallDistance >> 8;
			dataToSend[4] = overallDistance >> 16;
			dataToSend[5] = overallDistance >> 24;
			USBD_CUSTOM_HID_SendReport(&hUsbDeviceFS, dataToSend, 6);
			while(hhid->state != CUSTOM_HID_IDLE) osDelay(1);
			usbSyncProgress = 32;
			dataToSend[1] = USB_maxSpeed;
			dataToSend[2] = maxSpeed;	
			dataToSend[3] = 0;
			dataToSend[4] = 0;
			dataToSend[5] = 0;
			USBD_CUSTOM_HID_SendReport(&hUsbDeviceFS, dataToSend, 6);
			while(hhid->state != CUSTOM_HID_IDLE) osDelay(1);
			usbSyncProgress = 48;
			dataToSend[1] = USB_maxRpm;
			dataToSend[2] = maxRpm;
			USBD_CUSTOM_HID_SendReport(&hUsbDeviceFS, dataToSend, 6);
			while(hhid->state != CUSTOM_HID_IDLE) osDelay(1);
			usbSyncProgress = 64;
			dataToSend[1] = USB_maxDistance;
			dataToSend[2] = maxDistance;
			dataToSend[3] = maxDistance >> 8;
			USBD_CUSTOM_HID_SendReport(&hUsbDeviceFS, dataToSend, 6);
			while(hhid->state != CUSTOM_HID_IDLE) osDelay(1);
			usbSyncProgress = 80;
			dataToSend[1] = USB_maxDuration;
			dataToSend[2] = maxDuration;
			dataToSend[3] = maxDuration >> 8;
			USBD_CUSTOM_HID_SendReport(&hUsbDeviceFS, dataToSend, 6);
			while(hhid->state != CUSTOM_HID_IDLE) osDelay(1);
			usbSyncProgress = 100;
			xSemaphoreGive(accessUsbMutexHandle);
			break;
		}
		osDelay(100);
	}
}

void FlushEEPROMData()
{
	enableEEPROMWriting();
	writeEEPROMHalfWord(LAST_TRAVEL_DISTANCE_ADDRESS, lastTravelDistance);
	writeEEPROMHalfWord(LAST_TRAVEL_DURATION_ADDRESS, lastTravelDuration);
	writeEEPROMWord(OVERALL_DURATION_ADDRESS, overallDuration);
	writeEEPROMWord(OVERALL_DISTANCE_ADDRESS, overallDistance);
	writeEEPROMHalfWord(MAX_SPEED_ADDRESS, maxSpeed);
	writeEEPROMHalfWord(MAX_RPM_ADDRESS, maxRpm);
	writeEEPROMHalfWord(MAX_DISTANCE_ADDRESS, maxDistance);
	writeEEPROMHalfWord(MAX_DURATION_ADDRESS, maxDuration);
	writeEEPROMHalfWord(BACKLIGHT_TIME_ADDRESS, backlightTime);
	writeEEPROMHalfWord(AUTOSLEEP_TIME_ADDRESS, autosleepTime);
	disableEEPROMWriting();
}

void ClearStats(void)
{
	switch(prevState)
	{
		case STATE_OVERALL_STAT:
			overallDistance = 0;
			overallDuration = 0;		
			break;
		case STATE_STAT_SPEED:
			maxSpeed = 0;
			break;
		case STATE_STAT_RPM:
			maxRpm = 0;
			break;
		case STATE_STAT_DISTANCE:
			maxDistance = 0;
			break;
		case STATE_STAT_TIME:
			maxDuration = 0;
			break;
		case STATE_SETTINGS_CLEAR_STAT:
			overallDistance = 0;
			overallDuration = 0;		
			maxSpeed = 0;
			maxRpm = 0;
			maxDistance = 0;
			maxDuration = 0;
			break;
	}
	FlushEEPROMData();
}

void selectMenuItem(void)
{
	switch (TIM3->CNT % 6)
	{
		case 0:
			if (currentTravelStarted == 0)
				currentTravelStarted = sTime.Hours*3600+sTime.Minutes*60+sTime.Seconds;
			else
			{
				lastTravelDuration = currentTravelDuration;
				lastTravelDistance = currentTravelDistance;
				currentTravelStarted = 0;
			}			
			currentTravelDistance = 0;
			currentTravelDuration = 0;
			state = STATE_MAIN_SPEED;
			break;
		case 1:
			state = STATE_LAST_TRAVEL;
			break;
		case 2:
			state = STATE_OVERALL_STAT;
			break;
		case 3:
			state = STATE_STAT_SPEED;
			break;
		case 4:
			state = STATE_SYNC_WAIT;
			break;
		case 5:
			state = STATE_SETTINGS;
			break;
	}
}

void selectSettingsItem(void)
{
	switch (TIM3->CNT % 4)
	{
		case 0:
			state = STATE_SETTINGS_BACKLIGHT_MM;
			TIM3->CNT = backlightTime / 60;
			break;
		case 1:
			state = STATE_SETTINGS_AUTOSLEEP_MM;
			TIM3->CNT = autosleepTime / 60;
			break;
		case 2:
			prevState = STATE_SETTINGS_CLEAR_STAT;
			state = STATE_YES_NO;
			break;
		case 3:
			state = STATE_SETTINGS_SET_TIME_HH;
			tempTime = sTime.Hours * 60 + sTime.Minutes;
			TIM3->CNT = sTime.Hours;
			break;
	}
}

void processState(void)
{
	switch (state)
	{
		case STATE_MAIN_SPEED:
			if (back > 0)
			{
				back--;
				if (currentTravelStarted != 0)
					state = STATE_MAIN_DISTANCE;
			} else if (menu > 0)
			{
				menu--;
				state = STATE_MENU;
			}
			break;
		case STATE_MAIN_DISTANCE:
			if (back > 0)
			{
				back--;
				state = STATE_MAIN_SPEED;
			} else if (menu > 0)
			{
				menu--;
				state = STATE_MENU;
			}
			break;
		case STATE_MENU:
			if (back > 0)
			{
				back--;
				state = STATE_MAIN_SPEED;
			} else if (menu > 0)
			{
				menu--;
				selectMenuItem();
			}
			break;
		case STATE_LAST_TRAVEL:
			if (back > 0)
			{
				back--;
				state = STATE_MENU;
			}
			break;
		case STATE_OVERALL_STAT:
			if (back > 0)
			{
				back--;
				state = STATE_MENU;
			} else if (menu > 0)
			{
				menu--;
				prevState = STATE_OVERALL_STAT;
				state = STATE_YES_NO;
			}
			break;
		case STATE_STAT_SPEED:
			if (back > 0)
			{
				back--;
				prevState = STATE_STAT_SPEED;
				state = STATE_YES_NO;
			} else if (menu > 0)
			{
				menu--;
				state = STATE_STAT_RPM;
			}
			break;
		case STATE_STAT_RPM:
			if (back > 0)
			{
				back--;
				prevState = STATE_STAT_RPM;
				state = STATE_YES_NO;
			} else if (menu > 0)
			{
				menu--;
				state = STATE_STAT_DISTANCE;
			}
			break;
		case STATE_STAT_DISTANCE:
			if (back > 0)
			{
				back--;
				prevState = STATE_STAT_DISTANCE;
				state = STATE_YES_NO;
			} else if (menu > 0)
			{
				menu--;
				state = STATE_STAT_TIME;
			}
			break;
		case STATE_STAT_TIME:
			if (back > 0)
			{
				back--;
				prevState = STATE_STAT_TIME;
				state = STATE_YES_NO;
			} else if (menu > 0)
			{
				menu--;
				state = STATE_MENU;
			}
			break;
		case STATE_SYNC_WAIT:
			if (back > 0)
			{
				back--;
				state = STATE_MENU;
			} else {
				state = STATE_SYNC_RUNNING;
			}
			break;
		case STATE_SYNC_RUNNING:
			sendDataOverUSB();
			state = STATE_SYNC_COMPLETED;
			break;
		case STATE_SYNC_COMPLETED:
			if (back > 0)
			{
				back--;
				state = STATE_MENU;
			}
			break;
		case STATE_SETTINGS:
			if (back > 0)
			{
				back--;
				state = STATE_MENU;
			} else if (menu > 0)
			{
				menu--;
				selectSettingsItem();
			}
			break;
		case STATE_SETTINGS_BACKLIGHT_MM:
			if (back > 0)
			{
				back--;
				state = STATE_SETTINGS;
			} else if (menu > 0)
			{
				menu--;
				backlightTime = backlightTime % 60 + TIM3->CNT * 60;
				TIM3->CNT = backlightTime % 60;
				state = STATE_SETTINGS_BACKLIGHT_SS;
			}
			break;
		case STATE_SETTINGS_BACKLIGHT_SS:
			if (back > 0)
			{
				back--;
				state = STATE_SETTINGS;
			} else if (menu > 0)
			{
				menu--;
				backlightTime = backlightTime - (backlightTime % 60) + TIM3->CNT;
				state = STATE_SETTINGS;
			}
			break;
		case STATE_SETTINGS_AUTOSLEEP_MM:
			if (back > 0)
			{
				back--;
				state = STATE_SETTINGS;
			} else if (menu > 0)
			{
				menu--;
				autosleepTime = autosleepTime % 60 + TIM3->CNT * 60;
				TIM3->CNT = autosleepTime % 60;
				state = STATE_SETTINGS_AUTOSLEEP_SS;
			}
			break;
		case STATE_SETTINGS_AUTOSLEEP_SS:
			if (back > 0)
			{
				back--;
				state = STATE_SETTINGS;
			} else if (menu > 0)
			{
				menu--;
				autosleepTime = autosleepTime - (autosleepTime % 60) + TIM3->CNT;
				state = STATE_SETTINGS;
			}
			break;
		case STATE_SETTINGS_CLEAR_STAT:
			//nothing to do here, using YES_NO
			break;
		case STATE_SETTINGS_SET_TIME_HH:
			if (back > 0)
			{
				back--;
				state = STATE_SETTINGS;
			} else if (menu > 0)
			{
				menu--;
				tempTime = (tempTime % 60) + TIM3->CNT * 60;
				TIM3->CNT = tempTime % 60;
				state = STATE_SETTINGS_SET_TIME_MM;
			}
			break;
		case STATE_SETTINGS_SET_TIME_MM:
			if (back > 0)
			{
				back--;
				state = STATE_SETTINGS;
			} else if (menu > 0)
			{
				menu--;
				sTime.Hours = tempTime / 60;
				sTime.Minutes = TIM3->CNT;
				sTime.Seconds = 0;
				if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN) != HAL_OK)
				{
					Error_Handler();
				}
				if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK)
				{
					Error_Handler();
				}
				state = STATE_SETTINGS;
			}
			break;
		case STATE_YES_NO:
			if (back > 0)
			{
				back--;
				if (prevState == STATE_SETTINGS_CLEAR_STAT)
					state = STATE_SETTINGS;
				else
					state = prevState;
			} else if (menu > 0)
			{
				menu--;
				ClearStats();
				if (prevState == STATE_SETTINGS_CLEAR_STAT)
					state = STATE_SETTINGS;
				else
					state = prevState;
			}
			break;
		}
		back = 0;
		menu = 0;
}


void DrawMainContent(void)
{ 
	char* leftBut; char* rightBut; 	static uint8_t animation = 0;
	animation++; if (animation >= 100) animation = 0;
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, animation%10>8?1:0);
	PCF8812_Fill(0x00);
	switch (state)
	{
		case STATE_MAIN_SPEED:
			DrawMainScreenSpeed(Speed, frontRPM);
			leftBut = "menu";
			if (currentTravelStarted != 0)
				rightBut = "switch";
			else
				rightBut = "";
			break;
		case STATE_MAIN_DISTANCE:
			DrawScreenTravel("Current travel:", currentTravelDistance, currentTravelDuration);
			leftBut = "menu";
			rightBut = "switch";
			break;
		case STATE_MENU:
			if (currentTravelStarted == 0)
				DrawMenuList(MENU_MAIN, TIM3->CNT % 6);
			else
				DrawMenuList(MENU_MAIN_IN_TRAVEL, TIM3->CNT % 6);
			leftBut = "select";
			rightBut = "back";
			break;
		case STATE_LAST_TRAVEL:				
			DrawScreenTravel("Last travel:", lastTravelDistance, lastTravelDuration);
			leftBut = "";
			rightBut = "back";
			break;
		case STATE_OVERALL_STAT:				
			DrawScreenTravel("Overall stat:", overallDistance, overallDuration);
			leftBut = "clear";
			rightBut = "back";
			break;
		case STATE_STAT_SPEED:				
			DrawStatScreen("Max speed", maxSpeed, STAT_SPEED);
			leftBut = "next";
			rightBut = "clear";
			break;
		case STATE_STAT_RPM:				
			DrawStatScreen("Max RPM", maxRpm, STAT_RPM);
			leftBut = "next";
			rightBut = "clear";
			break;
		case STATE_STAT_DISTANCE:				
			DrawStatScreen("Max distance", maxDistance, STAT_DISTANCE);
			leftBut = "next";
			rightBut = "clear";
			break;
		case STATE_STAT_TIME:
			DrawStatScreen("Max travel time", maxDuration, STAT_TIME);
			leftBut = "menu";
			rightBut = "clear";
			break;
		case STATE_SYNC_WAIT:
			DrawUSBSyncTitleScreen();
			leftBut = "";
			rightBut = "back";
			break;
		case STATE_SYNC_RUNNING:
			DrawUSBSyncProgressScreen(usbSyncProgress);
			leftBut = "";
			rightBut = "";
			break;
		case STATE_SYNC_COMPLETED:
			DrawUSBSyncDoneScreen();
			leftBut = "";
			rightBut = "OK";
			break;
		case STATE_SETTINGS:
			DrawMenuList(MENU_SETTINGS, TIM3->CNT % 4);
			leftBut = "select";
			rightBut = "back";
			break;
		case STATE_SETTINGS_BACKLIGHT_MM:
			DrawTimeDialog("Baclkight time", TIM3->CNT, backlightTime % 60, animation%10>8?1:0);
			leftBut = "next";
			rightBut = "back";
			break;
		case STATE_SETTINGS_BACKLIGHT_SS:
			DrawTimeDialog("Baclkight time", backlightTime / 60, TIM3->CNT, animation%10>8?2:0);
			leftBut = "set";
			rightBut = "back";
			break;
		case STATE_SETTINGS_AUTOSLEEP_MM:
			DrawTimeDialog("Autosleep time", TIM3->CNT, autosleepTime % 60, animation%10>8?1:0);
			leftBut = "next";
			rightBut = "back";
			break;
		case STATE_SETTINGS_AUTOSLEEP_SS:
			DrawTimeDialog("Autosleep time", autosleepTime / 60, TIM3->CNT, animation%10>8?2:0);
			leftBut = "set";
			rightBut = "back";
			break;
		case STATE_SETTINGS_CLEAR_STAT:
			//drawing YES_NO instead
			break;
		case STATE_SETTINGS_SET_TIME_HH:
			DrawTimeDialog("Set time", TIM3->CNT, tempTime % 60, animation%10>8?1:0);
			leftBut = "next";
			rightBut = "back";
			break;
		case STATE_SETTINGS_SET_TIME_MM:
			DrawTimeDialog("Set time", tempTime / 60, TIM3->CNT, animation%10>8?2:0);
			leftBut = "set";
			rightBut = "back";
			break;
		case STATE_YES_NO:
			DrawYesNoDialogScreen(prevState);
			leftBut = "YES";
			rightBut = "NO";
			break;
	}
	
	DrawTopBar(sTime.Hours, sTime.Minutes, sTime.Seconds, wkup);
	DrawBotBar(leftBut, rightBut);
	//PCF8812_Fill(0x00);
	//DrawTestScreen();
	PCF8812_Flush();
}
/* USER CODE END 4 */

/* StartDefaultTask function */
void StartDefaultTask(void const * argument)
{
  /* USER CODE BEGIN 5 */	
	TIM3->CNT = 0;
	HAL_TIM_Encoder_Start(&htim3,TIM_CHANNEL_ALL);  
	osStatus status = osTimerStart(countSpeedTimerHandle, 1000);
	status = osTimerStart(everySecondTimerHandle, 1000);	
	
	//load persist data
	lastTravelDistance = readEEPROMHalfWord(LAST_TRAVEL_DISTANCE_ADDRESS);
	lastTravelDuration = readEEPROMHalfWord(LAST_TRAVEL_DURATION_ADDRESS);
	overallDuration = readEEPROMWord(OVERALL_DURATION_ADDRESS);
	overallDistance = readEEPROMWord(OVERALL_DISTANCE_ADDRESS);
	maxSpeed = readEEPROMHalfWord(MAX_SPEED_ADDRESS);
	maxRpm = readEEPROMHalfWord(MAX_RPM_ADDRESS);
	maxDistance = readEEPROMHalfWord(MAX_DISTANCE_ADDRESS);
	maxDuration = readEEPROMHalfWord(MAX_DURATION_ADDRESS);
	backlightTime = readEEPROMHalfWord(BACKLIGHT_TIME_ADDRESS);
	autosleepTime = readEEPROMHalfWord(AUTOSLEEP_TIME_ADDRESS);
	
	//osDelay(1500);
	
	while(1) {
		if (xSemaphoreTake(menuPressedSemaphoreHandle, ( TickType_t ) 1) == pdTRUE ) menu++;
		if (xSemaphoreTake(backPressedSemaphoreHandle, ( TickType_t ) 1) == pdTRUE ) back++;
		
		//if (back > 0 || menu > 0)
			processState();
		
		DrawMainContent();
		osDelay(200);
	}
  /* USER CODE END 5 */ 
}

/* UsbTask function */
void UsbTask(void const * argument)
{
  /* USER CODE BEGIN UsbTask */
  static unsigned char dataToSend[6];
	dataToSend[0] = 0x02;	
	dataToSend[1] = 0xff;
	dataToSend[2] = 0x00;
	dataToSend[3] = 0x00;
	dataToSend[4] = 0x00;
	dataToSend[5] = 0x00;
	/* Infinite loop */
  while (1)
  {
		if (xSemaphoreTake(accessUsbMutexHandle, (TickType_t) 10) == pdTRUE)
		{
			dataToSend[2] = (uint16_t)Speed;
			dataToSend[3] = (uint16_t)Speed >> 8;
			dataToSend[4] = frontRPM;
			dataToSend[5] = frontRPM >> 8;
			USBD_CUSTOM_HID_SendReport(&hUsbDeviceFS, dataToSend, 6);
			xSemaphoreGive(accessUsbMutexHandle);
		}
		osDelay(1000);
  }
  /* USER CODE END UsbTask */
}



/* countSpeedTimeElapsed function */
void countSpeedTimeElapsed(void const * argument)
{
  /* USER CODE BEGIN countSpeedTimeElapsed */
  while(1)
	{
		if (xSemaphoreTake(accessSpeedMutexHandle, (TickType_t) 1) == pdTRUE)
		{
			current = xTaskGetTickCount();
			HAL_NVIC_DisableIRQ(EXTI3_IRQn);
			rearLastFixed = rearLast1;
			HAL_NVIC_EnableIRQ(EXTI3_IRQn);
			HAL_NVIC_DisableIRQ(EXTI2_IRQn);
			frontLastFixed = frontLast1;
			HAL_NVIC_EnableIRQ(EXTI2_IRQn);
			if (current - rearLast > 5000)
			{
				rearLast = 0;
				rearRPM = 0;
				Speed = 0;
				rearLast1 = 0;
				rearLast2 = 0;
			} else {
				rearRPM = 60000 / rearLast1;
				Speed = WheelFactor / rearLast1;
				if (Speed > maxSpeed)
					maxSpeed = Speed;
			}
			if (current - frontLast > 5000)
			{
				frontLast = 0;
				frontRPM = 0;
				frontLast1 = 0;
				frontLast2 = 0;
			} else {
				frontRPM = 60000 / frontLast1;
				if (frontRPM > maxRpm)
					maxRpm = frontRPM;
			}			
			xSemaphoreGive(accessSpeedMutexHandle);
			break;
		}
		osDelay(10);		
	}
  /* USER CODE END countSpeedTimeElapsed */
}


/* everySecondTimeElapsed function */
void everySecondTimeElapsed(void const * argument)
{
/* USER CODE BEGIN everySecondTimeElapsed */
	HAL_RTC_GetTime(&hrtc, &sTime, FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &sDate, FORMAT_BIN);
	if (currentTravelStarted != 0)
	{
		TickType_t delta = currentTravelDuration;
		currentTravelDuration = (sTime.Seconds + sTime.Minutes*60 + sTime.Hours*3600) - currentTravelStarted;
		delta = currentTravelDuration - delta;
		overallDuration += delta;
		if (currentTravelDuration > maxDuration)
			maxDuration = currentTravelDuration;
	}
	if (HAL_GPIO_ReadPin(MenuButton_GPIO_Port, MenuButton_Pin) == 1)
		powerOffHold++;
	else
		powerOffHold = 0;
	if (powerOffHold >= 5)
		PowerOff();
	inactiveTime++;
	if (inactiveTime >= autosleepTime)
		GoToSleep();
	if (inactiveTime >= backlightTime)
	{		
		HAL_GPIO_WritePin(LCD_LED_PWR_GPIO_Port, LCD_LED_PWR_Pin, GPIO_PIN_RESET);
	} else if (HAL_GPIO_ReadPin(LCD_LED_PWR_GPIO_Port, LCD_LED_PWR_Pin) == GPIO_PIN_RESET)
	{		
		HAL_GPIO_WritePin(LCD_LED_PWR_GPIO_Port, LCD_LED_PWR_Pin, GPIO_PIN_SET);
	}
/* USER CODE END everySecondTimeElapsed */
}

void PowerOff(void)
{
	vTaskSuspendAll();
	PCF8812_Fill(0x00);
	DrawShutdownSplashScreen();
	PCF8812_Flush();
	FlushEEPROMData();
	while(HAL_GPIO_ReadPin(MenuButton_GPIO_Port, MenuButton_Pin) == 1) HAL_Delay(1);
	PCF8812_Fill(0x00);
	PCF8812_Flush();
	HAL_GPIO_WritePin(LCD_LED_PWR_GPIO_Port, LCD_LED_PWR_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LCD_PWR_GPIO_Port, LCD_PWR_Pin, GPIO_PIN_RESET);
	//enter standby mode
	RCC->APB1ENR |= RCC_APB1ENR_PWREN;//вкл тактирование PWR
	SCB->SCR |= SCB_SCR_SLEEPDEEP; //для M3 разрешаем sleepdeep
	SCB->SCR &= ~SCB_SCR_SLEEPONEXIT;
	PWR->CR |= PWR_CR_PDDS;//выбираем режим Power Down Deepsleep
	PWR->CR |= PWR_CR_CWUF ; //очищаем wakeup flag
	PWR->CSR |= PWR_CSR_EWUP; //разрешаем вэйкап, то есть пробуждение по переднему фронту на А0
	__WFE();  //уснули
}

void GoToSleep(void)
{
	;//wkup = 1;
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM1 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
/* USER CODE BEGIN Callback 0 */

/* USER CODE END Callback 0 */
  if (htim->Instance == TIM1) {
    HAL_IncTick();
  }
/* USER CODE BEGIN Callback 1 */

/* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
